﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    public enum MessageButtons {
        OK, 
        YesNo,
        Cancel
    }

    public enum MessageResult
    {
        OK,
        Yes,
        No,
        Cancel
    }
    public static void ShowMessage(string title, string message, MessageButtons buttons)
    {
        GameObject header = GameObject.FindGameObjectWithTag("Header");
        GameObject messageBox = GameObject.FindGameObjectWithTag("Message");
        header.GetComponent<Image>().enabled = true;
        messageBox.GetComponent<Image>().enabled = true;
        foreach(Transform go in header.transform)
        {
            go.gameObject.SetActive(true);
        }
        foreach(Transform go in messageBox.transform)
        {
            go.gameObject.SetActive(true);
        }
        header.GetComponentInChildren<TextMeshProUGUI>().text = title;
        messageBox.GetComponentInChildren<TextMeshProUGUI>().text = message;
        //switch (buttons)
        //{
        //    case MessageButtons.OK:

        //}
    }

    public static void Cancel()
    {
        GameObject header = GameObject.FindGameObjectWithTag("Header");
        GameObject messageBox = GameObject.FindGameObjectWithTag("Message");

        header.GetComponent<Image>().enabled = false;
        messageBox.GetComponent<Image>().enabled = false;
        foreach (Transform go in header.transform)
        {
            go.gameObject.SetActive(false);
        }
        foreach (Transform go in messageBox.transform)
        {
            go.gameObject.SetActive(false);
        }
    }
}
